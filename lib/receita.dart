import 'package:charts_flutter/flutter.dart' as charts;

class Receita {
  
  int id;
  String nome;
  double valor;
  int data;
  charts.Color color;

  Receita(this.nome, this.data, [this.valor=0.0, this.id=0, this.color]);
}