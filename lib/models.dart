//https://medium.com/@mohamedraja_77/sqflite-database-in-flutter-c0b7be83bcd2
import 'dart:async';
import 'dart:io' as io;
import 'package:bovespa_app/receita.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';

abstract class Model {

  static Database _db;

  Future<Database> get db async {
    if(_db != null)
      return _db;
    
    _db = await initDb();
    return _db;
  }

  initDb() async {
    io.Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "minhasfinancas2.db");
    var theDb = await openDatabase(path, version: 1, onCreate: _onCreate); //, onUpgrade: _onUpgrade );
    return theDb;
  }

  void _onCreate(Database db, int version) async {
    
    String sqlReceita = "CREATE TABLE IF NOT EXISTS Receita (id INTEGER PRIMARY KEY AUTOINCREMENT, descricao TEXT, data INTEGER, valor REAL)";
    await db.execute(sqlReceita).catchError((error) => print(error));

    String sqlDespesa = "CREATE TABLE IF NOT EXISTS Despesa (id INTEGER PRIMARY KEY AUTOINCREMENT, descricao TEXT, categoria TEXT,  data INTEGER, valor REAL)";
    await db.execute(sqlDespesa).catchError((error) => print(error));

    print('Tabelas criadas com sucesso!');      
  }

  // void _onUpgrade(Database db, int oldVersion, int version) async {
  //   String sql = "CREATE TABLE IF NOT EXISTS Receita(id INTEGER PRIMARY KEY AUTOINCREMENT, nome TEXT, valor REAL, mes INTEGER, ano INTEGER)";
  //   await db.execute(sql).catchError((error) => print(error));
  //   print('Tabela RECEITA atualizada com sucesso!');      
  // }
}

class ReceitaModel extends Model {
  
  Future<List<Receita>> read() async {

    var dbClient = await db;
    List<Map> list = await dbClient.rawQuery('SELECT * FROM Receita ORDER BY data desc');

    List<Receita> receitas = new List<Receita>();

    for (int i = 0; i < list.length; i++) {
      receitas.add(new Receita(list[i]["descricao"], list[i]["data"], list[i]["valor"], list[i]["id"]));
    }

    return receitas;
  }

  void create(Receita e) async {
    var dbClient = await db;
    await dbClient.transaction((txn) async {
      return await txn.rawInsert(
          'INSERT INTO Receita(descricao, data, valor) VALUES (?,?,?)',
          [e.nome, e.data, e.valor]);
    });
  }
}