import 'dart:async';

import 'package:bovespa_app/receita.dart';
import 'package:bovespa_app/receita_cadastro.dart';
import 'package:flutter/material.dart';
import 'models.dart';
// import 'package:flutter_calendar/calendar_tile.dart';
import 'package:charts_flutter/flutter.dart' as charts;

void main() => runApp(new MyApp());



class MyApp extends StatelessWidget {

   // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {

    return new MaterialApp(
      title: 'Flutter Demo',
      theme: new ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or press Run > Flutter Hot Reload in IntelliJ). Notice that the
        // counter didn't reset back to zero; the application is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: 
       new MyHomePage(title: 'Minhas Finanças'),
    );
  }
}

class MyHomePage extends StatefulWidget{
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;
  
  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

// https://google.github.io/charts/flutter/example/pie_charts/simple

class _MyHomePageState extends State<MyHomePage> with TickerProviderStateMixin {

  TabController tabController;
  int fabIndex;
  Color cor = Colors.blue;

  @override initState() { 
    super.initState();
     tabController = new TabController(length: 3, vsync: this, initialIndex: 0);
     fabIndex=0;
     tabController.addListener(() {
       setState(() {
                fabIndex = tabController.index;
                switch(fabIndex) {
                  case 0: cor = Colors.red; break;
                  case 1: cor = Colors.blue; break;
                  default: cor = Colors.green; break;
                }
              });
     });
     
  }
  
  

  void _incrementCounter() {
    setState(() {
          ReceitaModel().create(new Receita('Salário', DateTime.now().millisecondsSinceEpoch, 120.50));
        });
    
  }

  // https://stackoverflow.com/questions/46891916/flutter-change-main-appbar-title-on-other-pages
  // TODO: Colocar a cor azul e vermelha de acordo com o saldo final do mês/ano.

  // FloatActionButton
  // https://flutterdoc.com/insetting-fabs-within-the-bottomappbar-3b7fc18c9424

  // https://medium.com/@dev.n/flutter-challenge-whatsapp-b4dcca52217b

  // CRIAR TEXTFORMFIELD (RECEITA, DESPESA) E EM DASHBOARD EXIBIR GRÁFICO, ATUANDO SEMPRE COM DATA INÍCIO E DATA FIM!
  // DATETIMEPICKER!
String _value = '';

  Future _selectDate() async {
    DateTime picked = await showDatePicker(
        context: context,
        initialDate: new DateTime.now(),
        firstDate: new DateTime(2018),
        lastDate: new DateTime(DateTime.now().year + 1)
    );
    if(picked != null) setState(() => _value = picked.toString());
  }


Widget getFloatActionButton() { 
  
      switch(fabIndex) {
        case 0:
        // setState(() {
          // DynamicTheme.of(context).setBrightness(Brightness.dark);
          //   cor = Colors.red;      
          // });
          
          return new Container();
          
        break;

        case 1:
        // setState(() {
        //     cor = Colors.blue;      
        //   });
          return new FloatingActionButton(child: new Icon(Icons.add),onPressed: (){
            // _incrementCounter();
             Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => new ReceitaCadastro()));
          }, backgroundColor: cor,);
        break;

        default:
        // setState(() {
        //     cor = Colors.green;      
        //   });
          return FloatingActionButton(child: new Icon(Icons.add),onPressed: (){
            Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => new ReceitaCadastro()));
          }, backgroundColor: cor,);
        break;
      }
}


void dispose() {
   tabController.dispose();
   super.dispose();
 }

 Widget _dashboard() {

   List<Receita> _data;
   List<charts.Series<Receita, int>> _chartdata;

   _chartdata = new List<charts.Series<Receita, int>>();
   _data = <Receita> [
    new Receita('Receita', 1, 3000.0, 1, charts.MaterialPalette.blue.shadeDefault),
    new Receita('Despesa', 2, 2000.0, 2, charts.MaterialPalette.red.shadeDefault),
    // new Receita('13º', 3, 4000.0, 3, charts.MaterialPalette.green.shadeDefault),
   ];

   _chartdata.add(new charts.Series<Receita, int>(
     id: 'Receitas',
     data: _data,
     colorFn: (Receita receita, _) => receita.color,
     domainFn: (Receita receita, _) => receita.id,
     measureFn: (Receita receita, _) => receita.valor,
     labelAccessorFn: (Receita receita, _) => 'R\$${receita.valor}', 
   ));

   return Expanded (
     child: charts.PieChart(
       _chartdata,
       animate: true,
       animationDuration: new Duration(seconds: 1),
       defaultRenderer: new charts.ArcRendererConfig(arcRendererDecorators: [
          new charts.ArcLabelDecorator(
              labelPosition: charts.ArcLabelPosition.inside)
        ])
     ),);

 }



  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return DefaultTabController(
      
      length: 3,
      child: new Scaffold(
        appBar: new AppBar(
          backgroundColor: cor,
          // Here we take the value from the MyHomePage object that was created by
          // the App.build method, and use it to set our appbar title.
          title: new Text(widget.title),
          actions: <Widget>[
            IconButton(icon: Icon(Icons.calendar_today), onPressed: _selectDate,)
          ],
          bottom: TabBar(
              controller: tabController,
              tabs: [
                Tab(icon: Icon(Icons.dashboard), text: 'Visão Geral'),
                Tab(icon: Icon(Icons.monetization_on), text: 'Receitas',),
                Tab(icon: Icon(Icons.payment), text: 'Despesas',),
               
              ],
            ),
        ),
        body: TabBarView(
          controller: tabController,
                  children: [
                    new Column(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(20.0),
                                                  child: new Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              new Text("Receita\nR\$3000.00", style: TextStyle(color: Colors.blue, fontSize: 16.0),),
                              new Text("Despesa\nR\$1000.00", style: TextStyle(color: Colors.red, fontSize: 16.0),),
                              // new Text("Disponível\nR\$1000.00", style: TextStyle(color: Colors.green, fontSize: 16.0),)
                            ],
                          ),
                        ),
                        _dashboard(),
                      ],
                    ),
                    
                  // CalendarTile(date: DateTime.now(),),
                  new Container(
            padding: new EdgeInsets.all(10.0),
            child: new FutureBuilder<List<Receita>>(
              future: new ReceitaModel().read(),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return new ListView.builder(
                      itemCount: snapshot.data.length,
                      itemBuilder: (context, index) {

                        String data = DateTime.fromMillisecondsSinceEpoch(snapshot.data[index].data).day.toString()
                           + '/' + DateTime.fromMillisecondsSinceEpoch(snapshot.data[index].data).month.toString() 
                           + '/' + DateTime.fromMillisecondsSinceEpoch(snapshot.data[index].data).year.toString();
                        // data = data.substring(startIndex)
                        return GestureDetector(
                          onLongPress: (){ print("Ficou pressionado ${snapshot.data[index].nome}");},
                                                  child: Card(
                          
                                                      margin: const EdgeInsets.all(2.0),
                                                                                                        child: Padding(
                            padding: const EdgeInsets.all(12.0),
                              child: new Row(
                              
                              mainAxisSize: MainAxisSize.max,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  // Checkbox(
                                  //   value: true,
                                  //   onChanged: (_) { }
                                  // ),
                                  // new Text(data,
                                  //     style: new TextStyle(
                                  //         fontWeight: FontWeight.normal, fontSize: 14.0)),
                                  Expanded(
                                                                    child: Column(
                                                                      mainAxisAlignment: MainAxisAlignment.start,
                                                                      crossAxisAlignment: CrossAxisAlignment.start,
                                                                      children: [new Text(snapshot.data[index].nome,
                                        style: new TextStyle(
                                            fontWeight: FontWeight.bold, fontSize: 15.0)),
                                            new Text(data, style: TextStyle(fontSize: 12.0))
                                                                      ]),
                                  ),
                                  new Text('R\$ ' + snapshot.data[index].valor.toStringAsFixed(2),
                                      style: new TextStyle(
                                          fontWeight: FontWeight.normal, fontSize: 14.0)),
                                          
                                  // new Divider()
                                ]),
                                                    ),
                          ),
                        );
                      });
                } 
                // else if (snapshot.data.length == 0) {
                //   return new Text("No Data found");
                // }
                return new Container(alignment: AlignmentDirectional.center,child: new CircularProgressIndicator(),);
              },
            ),
          ),
          
          Center(child: Text("Despesas"))
         
          ],
        ),
        // floatingActionButton: fabs[fabIndex],
        floatingActionButton: getFloatActionButton()
        
        // new FloatingActionButton(          
        //   onPressed: _incrementCounter,
        //   tooltip: 'Increment',
        //   child: new Icon(Icons.add),
        // ),
        // floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked, // This trailing comma makes auto-formatting nicer for build methods.
      ),
    );
  }

}
