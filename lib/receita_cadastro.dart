import 'dart:async';

import 'package:bovespa_app/models.dart';
import 'package:bovespa_app/receita.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';

class ReceitaCadastro extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return new ReceitaCadastroState();
  }

}

// SafeArea
// new SafeArea(
//           top: false,
//           bottom: false,


class ReceitaCadastroState extends State<ReceitaCadastro> {

  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  Receita receita = new Receita("", DateTime.now().millisecondsSinceEpoch);

  //  final ValueChanged<DateTime> selectDate;
  DateTime selectedDate = DateTime.now();

  Future<void> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: new DateTime.now(),
      firstDate: DateTime(2015, 8),
      lastDate: DateTime(2101)
    );

    if(picked != null) { 
      setState(() {
              selectedDate = picked;
            });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: SafeArea(
        top: false,
        bottom: false,
              child: new Form(
          key: _formKey,
          autovalidate: true,
          child: new ListView(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            children: <Widget>[
              new TextFormField(
                decoration: const InputDecoration(
                icon: const Icon(Icons.description),
                hintText: 'De quem você recebeu?',
                labelText: 'Descrição',
                ),
                validator: (val) => val.isEmpty ? 'Description is required' : null,
                onSaved: (val) => receita.nome = val,
              ),
              new TextFormField(
                decoration: const InputDecoration(
                  icon: const Icon(Icons.monetization_on),
                  hintText: 'Quanto você recebeu, em reais?',
                  labelText: 'Valor',
                ), 
                             
                validator: (val) => val.isEmpty ? 'Valor is required' : null,
                onSaved: (val) => receita.valor = double.parse(val.replaceFirst(',', '.')),
                keyboardType: TextInputType.numberWithOptions(decimal: true, signed: false),
                    // inputFormatters: [
                    //   WhitelistingTextInputFormatter.,
                    // ],
              ),
              Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Icon(Icons.date_range, color: Colors.grey,),
              Text(DateFormat.yMMMd().format(selectedDate)),
              IconButton(icon: Icon(Icons.calendar_today),
                color: Theme.of(context).brightness == Brightness.light ? Colors.grey.shade700 : Colors.white70,
                onPressed: () { 
                  _selectDate(context);
                },
                
              ),
            ],
          
            ),
              new Container(
                padding: const EdgeInsets.only(left: 40.0, top: 20.0),
                child: new RaisedButton(
                  child: const Text('Salvar'),
                  onPressed: () {
                    FormState form = _formKey.currentState;
                    if (!form.validate()) {
                      showMessage('Form is not valid!  Please review and correct.');
                    } else {
                      form.save(); 
                      ReceitaModel model = new ReceitaModel();
                      receita.data = selectedDate.millisecondsSinceEpoch;
                      model.create(receita);
                      Navigator.of(context).pop();
                    }
                  },
                )
              ),
            ],
          )
        ),
      )
    );
  }

  void showMessage(String message, [MaterialColor color = Colors.red]) {
    _scaffoldKey.currentState
        .showSnackBar(new SnackBar(backgroundColor: color, content: new Text(message)));
  }

}